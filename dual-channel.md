# Create channel with TCP & UDP

## Server Config

### TCP config

File: /etc/init.d/tcp.conf

```
port 443
proto tcp
dev tun

ca ca.crt
cert server.crt
key server.key
dh dh1024.pem

ifconfig-pool-persist ipp.txt

server 10.67.0.0 255.255.0.0

#### client's option
push "route 10.240.0.0 255.255.0.0 vpn_gateway"

;auth-user-pass-verify auth-pam.pl via-file
auth-user-pass-verify auth-pam.js/bin/auth-pam.js via-file
client-cert-not-required
username-as-common-name

client-to-client
;duplicate-cn
keepalive 20 60

comp-lzo
max-clients 1000

persist-key
persist-tun

status openvpn-status.log
log-append openvpn.log

verb 3
mute 20

cipher BF-CBC
cipher AES-128-CBC
cipher DES-EDE3-CBC
```

### UDP config

File: /etc/openvpn/udp.conf

```
port 443
proto udp
dev tun

ca ca.crt
cert server.crt
key server.key
dh dh1024.pem

ifconfig-pool-persist ipp.txt

server 10.68.0.0 255.255.0.0

#### client's option
push "route 10.240.0.0 255.255.0.0 vpn_gateway"

#client fixed ip
#client-config-dir /etc/openvpn/ccd

;auth-user-pass-verify auth-pam.pl via-file
auth-user-pass-verify auth-pam.js/bin/auth-pam.js via-file
client-cert-not-required
username-as-common-name

client-to-client
;duplicate-cn
keepalive 20 60

comp-lzo
max-clients 1000

persist-key
persist-tun

status openvpn-status-udp.log
log-append openvpn-udp.log

verb 3
mute 20

cipher BF-CBC
cipher AES-128-CBC
cipher DES-EDE3-CBC
```

### Start servers

```
$ sudo /etc/init.d/openvpn start
```

## Client Configuration


### TCP Client Connection

File: client-tcp.conf

```
client
dev tun
proto tcp
remote 107.167.178.71 443
resolv-retry infinite
nobind
persist-key
persist-tun
ca ca.crt
auth-user-pass

keepalive 20 60
comp-lzo
verb 3
mute 20
route-method exe
route-delay 2

cipher BF-CBC
cipher AES-128-CBC
cipher DES-EDE3-CBC
```

### UDP Client Config

File: client-udp.conf

```
client
dev tun
proto udp
remote 107.167.178.71 443
resolv-retry infinite
nobind
persist-key
persist-tun
ca ca.crt
auth-user-pass

keepalive 20 60
comp-lzo
verb 3
mute 20
route-method exe
route-delay 2

cipher BF-CBC
cipher AES-128-CBC
cipher DES-EDE3-CBC
```

### Start client

```
$ sudo openvpn --config client-tcp.conf
$ sudo openvpn --config client-udp.conf
```


## iptables設定

File: /etc/sysconfig/iptables

```
# Firewall configuration written by system-config-firewall
# Manual customization of this file is not recommended.
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p udp -j ACCEPT
#-A INPUT -i tun+ -p udp -m udp --dport 443 -j ACCEPT

-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT
-A INPUT -m state --state NEW -m udp -p udp --dport 443 -j ACCEPT

-A INPUT -i tun+ -j ACCEPT
-A OUTPUT -o tun+ -j ACCEPT
-A OUTPUT -o eth0 -j ACCEPT
-A FORWARD -i tun+ -o eth0 -j ACCEPT
-A FORWARD -i eth0 -o tun+ -j ACCEPT
-A FORWARD  -d 255.255.255.255 -j ACCEPT

COMMIT
*nat
:PREROUTING ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]

-A POSTROUTING -s 10.67.0.0/24 -o eth0 -j MASQUERADE
-A POSTROUTING -s 10.68.0.0/24 -o eth0 -j MASQUERADE
COMMIT
```

### Restart iptables

```
$ sudo /etc/init.d/iptables restart
```